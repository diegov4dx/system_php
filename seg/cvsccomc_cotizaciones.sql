-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 25-11-2019 a las 00:58:13
-- Versión del servidor: 5.6.43
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cvsccomc_cotizaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_laboral`
--

CREATE TABLE `area_laboral` (
  `id` int(11) NOT NULL,
  `nombre_area` varchar(60) NOT NULL,
  `encargado` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area_laboral`
--

INSERT INTO `area_laboral` (`id`, `nombre_area`, `encargado`) VALUES
(1, 'todas', NULL),
(2, 'Tecnologia', NULL),
(3, 'Comercial', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `id` int(11) NOT NULL,
  `ref_articulo` varchar(60) NOT NULL,
  `nombre_articulo` varchar(300) NOT NULL,
  `precio_articulo` bigint(20) NOT NULL,
  `tipo_unidad` int(11) NOT NULL,
  `marca_articulo` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`id`, `ref_articulo`, `nombre_articulo`, `precio_articulo`, `tipo_unidad`, `marca_articulo`, `categoria`, `descripcion`) VALUES
(2, 'INFRARROJO ANTIMASCOTA ', 'INFRARROJO SENCILLO', 25900, 8, 6, 8, 'LC100PI'),
(4, 'INFRA DT ', 'INFRARROJO DOBLE TECNOLOGIA', 70900, 8, 6, 8, 'LC104'),
(5, 'INFRA DT', 'INFRARROJO DOBLE TECNOLOGIA ', 94900, 8, 6, 8, 'LC103'),
(6, 'PANEL DSC', 'PANEL DE ALARMA  1832', 123500, 8, 6, 8, 'PC1832'),
(7, 'PANEL DSC', 'PANEL DE ALARMA  1864', 184900, 8, 6, 8, 'PC1864'),
(8, 'MODULO EXPANSOR ', 'MODULO EXPANSOR  DE 8 ZONAS ', 79900, 8, 6, 8, 'PC5108'),
(9, 'KIT GSP 2', 'KIT DSC COMPLETO ', 328500, 8, 6, 8, 'PC1832 '),
(10, 'TECLADO', 'TECLADO LCD  8 ZONAS, 64', 89900, 8, 6, 8, 'PK5501'),
(11, '1864', 'KIT 1832 GSP 2', 328500, 8, 6, 8, '1864'),
(12, '1832', 'PANEL 1832', 123500, 8, 6, 8, '1832'),
(13, '1864', 'PANEL 1864', 184900, 8, 6, 8, '1864'),
(14, 'PK5501', 'TECLADO LCD PK5501', 89900, 8, 6, 8, 'PK5501'),
(15, 'PK5511', 'TECLADO LCD PK5511', 84900, 8, 6, 8, 'PK5511'),
(16, 'LC100PI', 'INFRARROJO ANTIMASCOTA SENCILLO', 22900, 8, 6, 8, 'LC100PI'),
(17, 'LC103', 'INFRARROJO DOBLE TECNOLOGIA ', 94900, 8, 6, 8, 'LC103'),
(18, 'LC104', 'INFRARROJO DOBLE TECNOLOGIA ', 66500, 8, 6, 8, 'LC104'),
(19, 'DG50AU', 'DISCRIMINADOR DE AUDIO ', 42900, 8, 6, 8, 'DG50AU'),
(20, 'SS-102', 'DISCRIMINADOR DE AUDIO ', 85500, 8, 6, 8, 'SS-102'),
(21, 'PC5108', 'MODULO DE EXPANSOR DE 8 ZONAS PARA POWER', 79900, 8, 6, 8, 'PC5108'),
(22, 'LC-151', 'INFRARROJO DOBLE TECNOLOGIA ', 197900, 8, 6, 8, 'LC-151'),
(23, 'WS9045', 'PANEL INALAMBRICO WS9045', 450900, 8, 6, 8, 'WS9045'),
(24, 'W4975', 'MAGNETICO INALAMBRICO PLANO', 74900, 8, 6, 8, 'W4975'),
(25, 'WS4945', 'MAGNETICO INALAMBRICO GRANDE ', 85900, 8, 6, 8, 'WS4945'),
(26, 'SM-226LQ', 'MAGNETICO PESADO ', 28900, 8, 6, 8, 'SM-226LQ'),
(27, 'WS4916', 'DETECTOR DE HUMO FOTOELECTRICO INALAMBRICO 433 MHZ', 210500, 8, 6, 8, 'WS4916'),
(28, 'WS4904PM', 'INFRARROJO INALAMBRICO ', 118900, 8, 6, 8, 'WS4904PM'),
(29, 'WS4939/43', 'LLAVERO INALAMBRICO 4 BOTONES', 89900, 8, 6, 8, 'WS4939/43'),
(30, 'SD20W', 'SIRENA 20W TIPO CORNETA,DOBLETONO', 14900, 8, 6, 8, 'SD20W'),
(31, 'SD30W', 'SIRENA 30W TIPO CORNETA,DOBLE TONO', 28900, 8, 6, 8, 'SD30W'),
(32, 'PL4.512', 'BATERIA DE ACIDO SELLADA 12VDC- 4.5AH', 28900, 8, 8, 8, 'PL4.512'),
(33, 'PL712', 'BATERIA DE ACIDO SELLADA 12VDC- 7AH', 34900, 8, 8, 8, 'PL712'),
(34, 'GAB GRA', 'DSC. CAJA METALICA GRANDE', 21900, 8, 9, 8, 'GAB GRA'),
(35, 'GAB PEQ', 'DSC. CAJA METALICA PEQUENA', 15900, 8, 9, 8, 'GAB PEQ'),
(36, 'SS-040Q/W', 'VIBROSENSOR MARAQUITA', 9500, 8, 10, 8, 'SS-040Q/W'),
(37, 'ST-1206-1.5A', 'FUENTE DE PODER 12 VOLTIOS 1.5AMPERIOSFUENTE DE PODER 12 VOLTIOS 1.5AMPERIOSFUENTE DE PODER 12 VOLTIOS 1.5AMPERIOS', 30500, 8, 10, 8, 'ST-1206-1,5A'),
(38, 'ST-2406-3AQ', 'FUENTE DE PODER 12 VOLTIOS 3 AMPERIOSFUENTE DE PODER 12 VOLTIOS 3AMPERIOSFUENTE DE PODER 12 VOLTIOS 3AMPERIOS', 87900, 8, 10, 8, 'ST-2406-3AQ'),
(39, 'ST-2406-2AQ', 'FUENTE DE PODER 12 VOLTIOS 2AMPERIOSFUENTE DE PODER 12 VOLTIOS 2AMPERIOSFUENTE DE PODER 12 VOLTIOS 2AMPERIOS', 35500, 8, 10, 8, 'ST-2406-2AQ'),
(40, 'ST-2406-5AQ', 'FUENTE DE PODER 12 VOLTIOS 5AMPERIOSFUENTE DE PODER 120VOLTIOS 5AMPERIOSFUENTE DE PODER 12 VOLTIOS 5AMPERIOS', 120900, 8, 10, 8, 'ST-2406-5AQ'),
(41, 'SS075CQ', 'PULSADOR DE EMERGENCIA NO/NC.', 5700, 8, 10, 8, 'SS075CQ'),
(42, 'SS075Q', 'PULSADOR DE EMERGENCIA ALAMBRICO N/O', 2900, 8, 10, 8, 'SS075Q'),
(43, 'SS072Q', 'TAMPER SWITCH PARA GABINETE', 5200, 8, 10, 8, 'SS072Q'),
(44, 'SS-078Q', 'BOTON DE PANICO TIPO LLAVE ', 21900, 8, 10, 8, 'SS-078Q'),
(45, 'SF119412', 'DETECTOR FOTOELECTRICO DE 4 HILOS ', 47900, 8, 11, 8, 'SF119412'),
(46, 'C4WBA', 'DETECTOR FOTOELECTRICO DE 4 HILOS ', 99900, 8, 11, 8, 'C4WBA'),
(47, 'SF2031', 'MAGNETICO LIVIANO ', 3900, 8, 11, 8, 'SF2031'),
(48, 'SF3012', 'MAGNETICO PESADO ', 27900, 8, 11, 8, 'SF3012'),
(49, 'SF2041', 'MAGNETICO SEMI', 9900, 8, 11, 8, 'SF2041'),
(50, 'PS1640', 'TRANSFORMADOR DE PARED DE 16.5 VCA', 17900, 8, 11, 8, 'PS1640'),
(51, 'SF100P', 'DETECTOR DE RAYO FOTOELECTRICO / 2 RAYOS / 100MTRS', 137900, 8, 11, 8, 'SF100P'),
(52, 'SL- 126Q/B', 'LUZ ESTROBOSCOPICA SECOLARM SL- 126Q/B (AZUL)', 41900, 8, 11, 12, 'SL- 126Q/B'),
(53, 'SL- 126Q/R', 'LUZ ESTROBOSCOPICA SECOLARM SL- 126Q/R (ROJO)', 41900, 8, 11, 12, 'SL- 126Q/R'),
(54, 'SL- 126Q/G', 'LUZ ESTROBOSCOPICA SECOLARM SL- 126Q/G (VERDE)', 41900, 8, 11, 12, 'SL- 126Q/G'),
(55, 'AB355NXT01', 'CAJA CABLE UTP CAT5E - 305 MTS', 1100, 8, 12, 5, 'AB355NXT01'),
(56, 'AB356NXT01', 'CAJA CABLE UTP CAT 6 ? 305 MTS GRIS/AZUL', 2500, 8, 12, 5, 'AB356NXT01'),
(57, 'LD7TURBOW', 'CAMARA DOMO PARA INTERIOR 720P TURBOHD CON LENTE FIJO 2.8MM, IR 20M, SERIE LEGEND TURBOHD CAMARA DOMO PARA INTERIOR 720P TURBOHD CON LENTE FIJO 2.8MM, IR 20M, SERIE LEGEND TURBOHD', 52900, 8, 8, 10, 'LD7TURBOW'),
(58, 'LE7TURBOW', 'CAMARA EYEBALL ANTIVANDALISMO LEGEND TURBOHD 720P CON LENTE FIJO 2.8MM E IR INTELIGENTE PARA 20MCAMARA EYEBALL ANTIVANDALISMO LEGEND TURBOHD 720PCON LENTE FIJO 2.8MM E IR INTELIGENTE PARA 20M', 59900, 8, 8, 10, 'LE7TURBOW'),
(59, 'LE7TURBOV', 'CAMARA TURRET LEGEND TURBOHD 720P HIBRIDA (ANALOGICO 1200TVL / HD-TVI 720P) LENTE 2.8 12MM E IR INTELIGENTE PARA 40MCAMARA TURRET LEGEND TURBOHD 720P HIBRIDA (ANALOGICO 1200TVL / HD-TVI 720P) LENTE 2.8 12MM E IR INTELIGENTE PARA 40M', 149900, 8, 8, 10, 'LE7TURBOV'),
(60, 'LB7TURBOW', 'CAMARA BALA 720P TURBOHD CON LENTE FIJO 3.6MM, IR 20M, COLOR BLANCO, SERIE LEGEND TURBOHD  LB7TURBO/ LB7TURBOWCAMARA BALA 720P TURBOHD CON LENTE FIJO 3.6MM, IR 20M, COLOR BLANCO, SERIE LEGEND TURBOHD  LB7TURBO/ LB7TURBOW', 59900, 8, 8, 10, 'LB7TURBOW'),
(61, 'LB7TURBOVW', 'CAMARA BALA LEGEND TURBOHD 720P HIBRIDA (ANALOGICO 1200TVL / HD-TVI 720P) LENTE 2.8 ? 12MM E IR INTELIGENTE PARA 20M', 139900, 8, 8, 10, 'LB7TURBOVW'),
(62, 'H7TURBO', 'CAMARA OCULTA EN SENSOR DE MOVIMIENTO TURBOHD HD-TVI (720P)', 92900, 8, 8, 10, 'H7TURBO'),
(63, 'P7TURBODN', 'CAMARA TURBOHD (720P), TIPO PINHOLE, LENTE 3.7 MM, IDEAL PARA OCULTARSE, DIA/NOCHE', 87900, 8, 8, 10, 'P7TURBODN'),
(64, 'DS2CE16D5TVFIT3', 'CAMARA TIPO BALA EXIR 1080P TURBOHD CON LENTE VARIFOCAL 2.8 A 12 MM, IR 50M Y WDR REAL', 350900, 8, 8, 10, 'DS2CE16D5TVFIT3'),
(65, 'B8TURBOXW', 'CAMARA BALA 1080P TURBOHD, GRAN ANGULAR(LENTE 2.8MM), IR 20M, WDR EXTREMO', 89900, 8, 8, 10, 'B8TURBOXW'),
(66, 'B8TURBOVZW', 'CAMARA BALA TURBO-HD 2MP, LENTE MOTORIZADO 2.8 - 12MM SMARTIR EXIR 40MTS, WDR, INTERIOR/EXTERIOR IP66 COLOR BLANCO', 210900, 8, 8, 10, 'B8TURBOVZW'),
(67, 'B8TURBOEXIR28W', 'CAMARA BULLET TURBOHD 1080P CON LENTE FIJO DE 2.8MM E IR INTELIGENTE CON DOBLE EXIR PARA 80M. COLOR BLANCO', 158900, 8, 8, 10, 'B8TURBOEXIR28W'),
(68, 'B8TURBOEXIR2W', 'CAMARA BULLET TURBOHD 1080P CON LENTE FIJO DE 3.6MM E IR INTELIGENTE CON DOBLE EXIR PARA 80M. COLOR BLANCO', 158900, 8, 8, 10, 'B8TURBOEXIR2W'),
(69, 'B8TURBOVIRW', 'CAMARA BULLET TURBOHD 1080P CON LENTE VARIFOCAL DE 2.8 ? 12MM E IR INTELIGENTE PARA 50MTS. COLOR BLANCO', 199900, 8, 8, 10, 'B8TURBOVIRW'),
(70, 'D8TURBOW', 'DOMO IR 1080P TURBOHD CON LENTE 2.8 MM E IR 20 M', 80900, 8, 8, 10, 'D8TURBOW'),
(71, 'E8TURBO', 'CAMARA EYEBALL 1080P TURBOHD CON LENTE GRAN ANGULO 2.8MM(90 GRADOS) E IR 20M', 89900, 8, 8, 10, 'E8TURBO'),
(72, 'DS2AE7230TIA', 'DOMO PTZ TURBOHD 1080P, 30X ZOOM OPTICO, 120 M ILUMINACION IR, DWDR, IP66 PARA EXTERIOR', 2154900, 8, 8, 10, 'DS2AE7230TIA'),
(73, 'DS-2AE4123TA', 'DOMO PTZ 23X / 720P CON DWDR', 829900, 8, 8, 10, 'DS-2AE4123TA'),
(74, 'S04TURBOX', 'VIDEOGRABADORAS DIGITALES STANDALONE 04  CANALES', 139900, 8, 8, 10, 'S04TURBOX'),
(75, 'S08TURBOX', 'VIDEOGRABADORAS DIGITALES STANDALONE 08 CANALES', 209900, 8, 8, 10, 'S08TURBOX'),
(76, 'S16TURBOX', 'VIDEOGRABADORAS DIGITALES STANDALONE 16 CANALES', 319900, 8, 8, 10, 'S16TURBOX'),
(77, 'EV1004TURBOX', 'DVRS TURBOHD TRIBRIDAS (TVI/ANALOGICO/IP) 1080P 4 CANALES', 145900, 8, 8, 10, 'EV1004TURBOX'),
(78, 'EV1008TURBOX', 'DVRS TURBOHD TRIBRIDAS (TVI/ANALOGICO/IP) 1080P 8 CANALES', 299900, 8, 8, 10, 'EV1008TURBOX'),
(79, 'EV1016TURBOX', 'DVRS TURBOHD TRIBRIDAS (TVI/ANALOGICO/IP) 1080P 16 CANALES', 569900, 8, 8, 10, 'EV1016TURBOX'),
(80, 'DS-7316HGHI-SH', 'DVRS TURBOHD TRIBRIDAS (TVI/ANALOGICO/IP) 1080P 16 CANALES', 1424900, 8, 8, 10, 'DS-7316HGHI-SH'),
(81, 'DS-7324HGHI-SH', 'DVRS TURBOHD TRIBRIDAS (TVI/ANALOGICO/IP) 1080P 24 CANALES', 1639900, 8, 8, 10, 'DS-7324HGHI-SH'),
(82, 'DS-7332HGHI-SH', 'DVRS TURBOHD TRIBRIDAS (TVI/ANALOGICO/IP) 1080P 32 CANALES', 2099900, 8, 8, 10, 'DS-7332HGHI-SH'),
(83, 'TT101PVTURBO', 'KIT DE TRANSCEPTORES CON ALIMENTACION (12V/24VCD/AC) TURBOHD', 24900, 8, 8, 10, 'TT101PVTURBO'),
(84, 'TT101FTURBO', 'KIT DE TRANSCEPTORES PASIVOS TURBOHD', 11900, 8, 8, 10, 'TT101FTURBO'),
(85, 'WD10PURX', 'DISCO DURO SATA 1TB SERIE WD PURPLE OPTIMIZADO PARA CCTV, 5400RPM, 24/7', 219900, 8, 8, 10, 'WD10PURX'),
(86, 'WD20PURX', 'DISCO DURO SATA 2TB SERIE WD PURPLE OPTIMIZADO PARA CCTV, 5400RPM, 24/7', 319900, 8, 8, 10, 'WD20PURX'),
(87, 'WD30PURX', 'DISCO DURO SATA 3TB SERIE WD PURPLE OPTIMIZADO PARA CCTV, 5400RPM, 24/7', 439900, 8, 8, 10, 'WD30PURX'),
(88, 'WD40PURX', 'DISCO DURO SATA 4TB SERIE WD PURPLE OPTIMIZADO PARA CCTV, 5400RPM, 24/7', 589900, 8, 8, 10, 'WD40PURX'),
(89, '12V1250AMP', 'FUENTES DE PODER DE 24 VCA/12 VCD', 12900, 8, 8, 10, '12V1250AMP'),
(90, 'ART. PRUEBA', 'ART. PRUEBA', 12000, 8, 6, 5, 'ART. PRUEBA'),
(91, '', '', 0, 0, 0, 0, ''),
(92, '', '', 0, 0, 0, 0, ''),
(93, '', '', 0, 0, 0, 0, ''),
(94, '', '', 0, 0, 0, 0, ''),
(95, 'Computador Portatil', 'Computador Portatil HP CORE i3 3ra Generacion', 1000000, 8, 8, 7, 'HP L200');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_app`
--

CREATE TABLE `categorias_app` (
  `id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias_app`
--

INSERT INTO `categorias_app` (`id`, `nombre`) VALUES
(1, 'Usuario'),
(2, 'Cliente'),
(3, 'Cotizacion'),
(4, 'Articulo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_articulo`
--

CREATE TABLE `categoria_articulo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_articulo`
--

INSERT INTO `categoria_articulo` (`id`, `nombre`, `descripcion`) VALUES
(5, 'ALARMA DE INSTRUCCION ', 'ALARMA DE INSTRUCCION '),
(6, 'CIRCUITO CERRADO DE TELEVISION ', 'CCTV'),
(7, 'CONTROL DE ACCESO ', 'CONTROL DE ACCESO '),
(8, 'SISTEMA DE ALARMA DE INTRUSION', 'SISTEMA DE ALARMA DE INTRUSION'),
(9, 'SISTEMA DE ALARMA DE INCENDIO', 'SISTEMA DE ALARMA DE INCENDIO'),
(10, 'SITEMA DE CIRCUITOCERRADO DE TELEVICION ', 'SITEMA DE CIRCUITOCERRADO DE TELEVICION '),
(11, 'SITEMA DE CONTROL DE ACCSESO ', 'SITEMA DE CONTROL DE ACCSESO '),
(12, 'SISTEMA DE EVACUACION ', 'SISTEMA DE EVACUACION '),
(13, 'EQUIPO COMPUTO', 'EQUIPOS DE COMPUTO'),
(14, 'Prueba', 'Prueba JQUERY');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion_detalle`
--

CREATE TABLE `cotizacion_detalle` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valor` double NOT NULL,
  `id_maestra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cotizacion_detalle`
--

INSERT INTO `cotizacion_detalle` (`id`, `id_producto`, `cantidad`, `valor`, `id_maestra`) VALUES
(6, 2, 1, 25900, 10),
(7, 4, 2, 70900, 11),
(8, 8, 1, 79900, 11),
(101, 85, 1, 219900, 46),
(10, 32, 1, 28900, 13),
(11, 44, 1, 21900, 13),
(12, 32, 1, 28900, 14),
(102, 33, 1, 34900, 15),
(14, 32, 1, 28900, 16),
(15, 32, 1, 28900, 17),
(16, 32, 1, 28900, 18),
(17, 32, 1, 28900, 19),
(18, 32, 1, 28900, 20),
(19, 32, 1, 28900, 21),
(20, 32, 1, 28900, 22),
(21, 32, 1, 28900, 23),
(22, 32, 1, 28900, 24),
(23, 32, 1, 28900, 25),
(24, 32, 1, 28900, 26),
(25, 44, 1, 21900, 27),
(26, 44, 1, 21900, 28),
(27, 44, 1, 21900, 29),
(28, 44, 1, 21900, 30),
(29, 32, 1, 28900, 31),
(30, 32, 1, 28900, 32),
(31, 32, 1, 28900, 33),
(32, 32, 1, 28900, 34),
(33, 32, 1, 28900, 35),
(34, 32, 1, 28900, 36),
(36, 60, 1, 59900, 38),
(37, 32, 1, 28900, 39),
(38, 32, 1, 28900, 40),
(39, 32, 1, 28900, 41),
(54, 90, 1, 12000, 37),
(48, 32, 1, 28900, 42),
(49, 60, 4, 59900, 42),
(109, 79, 1, 569900, 43),
(53, 66, 2, 210900, 37),
(67, 56, 2, 2500, 12),
(59, 56, 1, 2500, 44),
(58, 0, 2, 569900, 44),
(62, 32, 1, 28900, 45),
(100, 95, 1, 1000000, 46),
(74, 95, 1, 1000000, 47),
(75, 85, 1, 219900, 47),
(76, 95, 1, 1000000, 48),
(77, 85, 1, 219900, 48),
(103, 63, 1, 87900, 49),
(104, 33, 1, 34900, 49),
(108, 0, 1, 1100, 50),
(107, 32, 2, 100000, 50),
(110, 44, 1, 21900, 43),
(111, 33, 2, 34900, 51);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion_maestro`
--

CREATE TABLE `cotizacion_maestro` (
  `id` int(11) NOT NULL,
  `id_tercero` varchar(20) NOT NULL,
  `id_vendedor` bigint(20) NOT NULL,
  `concepto_detalle` text NOT NULL,
  `fecha_cotizacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL,
  `fecha_cambio_estado` date NOT NULL,
  `usuario_cambio_estado` bigint(20) NOT NULL,
  `categoria_asignada` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cotizacion_maestro`
--

INSERT INTO `cotizacion_maestro` (`id`, `id_tercero`, `id_vendedor`, `concepto_detalle`, `fecha_cotizacion`, `estado`, `fecha_cambio_estado`, `usuario_cambio_estado`, `categoria_asignada`) VALUES
(10, '1111111111', 1045677244, 'Cambio de dispositivo  ', '2018-08-25 14:26:28', 10, '0000-00-00', 0, 3),
(11, '1143452443', 1143452443, 'Prueba Cotizaci?n  Electronica', '2018-08-25 14:28:14', 9, '0000-00-00', 0, 3),
(12, '1143452443', 72336225, 'kjk ', '2018-12-26 19:29:34', 9, '0000-00-00', 1143452443, 3),
(13, '1143452443', 1143452443, 'prueba ', '2018-12-26 19:30:08', 10, '0000-00-00', 1143452443, 3),
(14, '1143452443', 1143452443, 'Prueba Mail ', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(15, '1143452443', 1143452443, '14456', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(16, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(17, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(18, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(19, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(20, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(21, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(22, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(23, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(24, '1143452443', 1143452443, '5646546', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(25, '1143452443', 1143452443, 'dgdhds', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(26, '1143452443', 1143452443, 'dgdhds', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(27, '1143452443', 1143452443, 'dgdhds', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(28, '1143452443', 1143452443, 'dgdhds', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(29, '1143452443', 1143452443, 'dgdhds', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(30, '1143452443', 1143452443, 'dgdhds', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(31, '1143452443', 1143452443, 'ssfs', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(32, '1143452443', 1143452443, 'ssfs', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(33, '1143452443', 1143452443, 'ssfs', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(34, '1143452443', 1143452443, 'ssfs', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(35, '1143452443', 1143452443, 'ssfs', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(36, '1143452443', 1143452443, '55465', '2018-08-24 22:51:17', 11, '2017-09-20', 1143452443, 4),
(37, '1143452443', 1143452443, 'dgdh', '2018-08-24 22:51:17', 11, '1900-01-01', 1143452443, 4),
(38, '1045686665', 1143452443, 'Prueba Prospecto', '2018-08-24 22:51:17', 11, '1900-01-01', 1143452443, 4),
(39, '1045729597', 1143452443, 'Prueba 2 Prospecto', '2018-08-25 14:21:03', 10, '0000-00-00', 0, 4),
(40, '54645654', 1143452443, 'Prueba 2 Prospecto', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(41, '546456542', 1143452443, 'Prueba 2 Prospecto', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(42, '5464565423', 1143452443, 'Prueba 2 Prospecto', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 4),
(45, '1143452443', 1143452443, 'Prueba', '2018-12-03 08:02:59', 11, '0000-00-00', 0, 0),
(43, '1143452443', 1143452443, 'prueba tablet ', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 0),
(44, '1143452443', 1143452443, 'instalacion cartagena', '2018-08-24 22:51:17', 11, '0000-00-00', 0, 0),
(46, '800101613', 1143452443, 'INSTALACIÓN CONTROL DE ACCESO.', '2018-12-26 09:50:58', 11, '0000-00-00', 0, 0),
(47, '800101613', 1143452443, 'INSTALACIÓN CONTROL DE ACCESO.', '2018-12-27 13:38:22', 9, '0000-00-00', 1143452443, 0),
(48, '800101613', 1143452443, 'INSTALACIÓN CONTROL DE ACCESO.', '2018-12-26 10:02:33', 11, '0000-00-00', 0, 0),
(49, '800101613', 1143452443, 'Compra de Alarmas', '2019-07-11 09:30:04', 11, '0000-00-00', 0, 0),
(50, '54645654', 1143452443, 'Prueba cotizacion electroinca', '2019-08-02 01:33:45', 11, '0000-00-00', 0, 0),
(51, '1143452443', 1143452443, 'Prueba Practica', '2019-10-23 05:45:26', 11, '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_cliente`
--

CREATE TABLE `c_cliente` (
  `id` int(11) NOT NULL,
  `apellidos` varchar(60) NOT NULL,
  `nombres` varchar(60) NOT NULL,
  `razon_social` varchar(120) NOT NULL,
  `nit_cedula` varchar(15) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `celular` bigint(20) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `prospecto` tinyint(1) NOT NULL DEFAULT '0',
  `estado` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `c_cliente`
--

INSERT INTO `c_cliente` (`id`, `apellidos`, `nombres`, `razon_social`, `nit_cedula`, `direccion`, `telefono`, `celular`, `ciudad`, `email`, `prospecto`, `estado`) VALUES
(1, 'Herrera Barrios', 'Danilo Andres', 'Prueba Cliente', '1143452443', 'Cra 12A#18-120', 3265285, 3043723261, 1, 'daniloherrera@cvsc.com.co', 0, 'A'),
(2, 'Cliente', 'Cliente', 'Prueba Cliente', '1111111111', 'Cliente#', 5464, 5464, 1, 'Dani2@vcsc.com.co', 1, 'A'),
(3, 'Cliente', 'Cliente', 'Prueba Cliente', '1045686665', 'Cliente#', 5464, 5464, 1, 'Dani2@vcsc.com.co', 1, 'I'),
(8, 'Cliente', 'Cliente', 'Prueba Cliente', '1045729597', 'Cliente#', 5464, 5464, 1, 'Dani2@vcsc.com.co', 1, 'A'),
(9, 'Niebles', 'Marlay', 'Marlay Niebles Riquett', '54645654', 'Cra 42 #75b-119', 25215154, 25215154, 1, 'marlayniebles@cvsc.com.co', 0, 'I'),
(10, 'SAS', 'LOGITEM ', 'LOGITEM SAS', '546456542', 'CRA 42# 75B-101', 5464, 5464, 1, 'ADMIN@LOGITEM.CO', 0, 'A'),
(11, 'Cliente', 'Cliente', 'Prueba Cliente', '5464565423', 'Cliente#', 5464, 5464, 1, 'Dani2@vcsc.com.co', 1, 'A'),
(14, 'LTDA', 'COLVISEG DEL CARIBE', 'COLVISEG DEL CARIVE LTDA', '800101613', 'CRA 12A# 18-120', 3265285, 3043723261, 0, 'ADMIN@CVSC.COMC.CO', 0, 'A'),
(13, 'GRAVIER ORTIZ', 'LILIANA', 'GRAVIER ORTIZ LILIANA', '1045683930', 'CRA 12A # 87 19 CIUDAD MODESTO', 3859124, 3016510795, 1, 'lilianagravier@logitem.co', 0, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_usuario`
--

CREATE TABLE `c_usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` char(15) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `login` varchar(1) COLLATE utf8_spanish2_ci DEFAULT 'i',
  `rol` int(11) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `c_usuario`
--

INSERT INTO `c_usuario` (`id`, `usuario`, `contrasena`, `estado`, `login`, `rol`) VALUES
(1, 'danilo', '123', 1, 'i', 1),
(2, 'marlay', '123', 1, 'i', 1),
(11, 'idiazp', 'idiazp', 0, 'i', 2),
(12, 'eartetap', 'eartetap', 0, 'i', 2),
(13, 'rvilarog', 'rvilarog', 0, 'i', 2),
(14, 'jdelgado', 'jdelgado', 0, 'i', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `categoria` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`, `categoria`) VALUES
(1, 'Activo', 1),
(4, 'Inactivo', 1),
(5, 'Activo', 2),
(6, 'Inactivo', 2),
(7, 'Activo', 4),
(8, 'Inactivo', 4),
(9, 'Aprobado Encargado', 3),
(10, 'Rechazado Encargado', 3),
(11, 'Por Aprobar Encargado', 3),
(12, 'Por Aprobar Cliente', 3),
(13, 'Aprobado Cliente', 3),
(14, 'Rechazado Cliente', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_articulo`
--

CREATE TABLE `marca_articulo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca_articulo`
--

INSERT INTO `marca_articulo` (`id`, `nombre`, `descripcion`) VALUES
(13, 'HP', 'Hewlett Packard'),
(6, 'DSC', 'DSC'),
(8, 'EPCOM', 'EPCOM'),
(9, 'GSP', 'GSP'),
(10, 'SECO LARM', 'SECO LARM'),
(11, 'S-FIRE', 'S-FIRE'),
(12, 'NEXXT', 'NEXXT'),
(14, 'EPSON', 'EPSON');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolusuario`
--

CREATE TABLE `rolusuario` (
  `id` int(11) NOT NULL,
  `rol` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rolusuario`
--

INSERT INTO `rolusuario` (`id`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Estandar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(12) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`id`, `nombre`, `descripcion`) VALUES
(7, 'Herramienta', 'Tipo Herramienta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_medida`
--

CREATE TABLE `unidad_medida` (
  `id` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidad_medida`
--

INSERT INTO `unidad_medida` (`id`, `nombre`, `descripcion`) VALUES
(8, 'UNIDAD', 'UNIDAD'),
(9, 'CAJA', 'CAJA'),
(10, 'PAR', 'PAR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` char(15) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `login` varchar(1) COLLATE utf8_spanish2_ci DEFAULT 'i',
  `rol` int(11) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `contrasena`, `estado`, `login`, `rol`) VALUES
(1, 'danilo', '123', 1, 'i', 1),
(2, 'marlay', '123', 1, 'i', 1),
(11, 'idiazp', 'idiazp', 0, 'i', 2),
(12, 'eartetap', 'eartetap', 0, 'i', 2),
(13, 'rvilarog', 'rvilarog', 0, 'i', 2),
(14, 'jdelgado', 'jdelgado', 0, 'i', 2),
(15, 'valerie', '123', 0, 'i', 2),
(16, '', '', 0, 'i', 2),
(17, 'angelyolo', 'angelyolo', 0, 'i', 2),
(18, 'danielmer', '123', 0, 'i', 2),
(19, 'danielmer', '123', 0, 'i', 2),
(20, 'daniloherr', '123', 0, 'i', 2),
(21, 'daniloherr', '123', 1, 'i', 2),
(22, 'daniloherr', '123', 1, 'i', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_detalle`
--

CREATE TABLE `usuario_detalle` (
  `id` int(11) NOT NULL,
  `apellidos` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `nombres` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `cedula` bigint(20) NOT NULL,
  `nacimiento` date NOT NULL DEFAULT '1999-01-01',
  `registro` date NOT NULL DEFAULT '1900-01-01',
  `usuario_log` int(11) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `area_lab` int(11) DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario_detalle`
--

INSERT INTO `usuario_detalle` (`id`, `apellidos`, `nombres`, `cedula`, `nacimiento`, `registro`, `usuario_log`, `telefono`, `area_lab`, `email`) VALUES
(1, 'Herrera Barrios', 'Danilo  Andres', 1143452443, '1995-08-15', '2017-03-15', 1, 3043723261, 1, 'correo@cvsc.com.co'),
(3, 'Diaz Peralta', 'Ismael  de Jesus', 1045677244, '1989-01-23', '2017-05-17', 11, 3135270349, 1, 'ismaeldiaz@cvsc.com.co'),
(4, 'Arteta Palacin ', 'Edgardo Enrique', 73132156, '1967-06-26', '2017-05-18', 12, 3114156457, 1, 'edgardoarteta@cvsc.com.co'),
(5, 'Vilaro Gutierrez', 'Rafael Antonio ', 72252286, '1980-08-11', '2017-05-18', 13, 0, 1, 'rafaelvilaro@cvsc.com.co'),
(6, 'Delgado Salgado', 'Juan Carlos', 72336225, '1985-05-06', '1900-01-01', 14, 3106321672, 1, 'juandelgado@cvsc.com.co');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area_laboral`
--
ALTER TABLE `area_laboral`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias_app`
--
ALTER TABLE `categorias_app`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_articulo`
--
ALTER TABLE `categoria_articulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cotizacion_detalle`
--
ALTER TABLE `cotizacion_detalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cotizacion_maestro`
--
ALTER TABLE `cotizacion_maestro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `c_cliente`
--
ALTER TABLE `c_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `c_usuario`
--
ALTER TABLE `c_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marca_articulo`
--
ALTER TABLE `marca_articulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rolusuario`
--
ALTER TABLE `rolusuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_detalle`
--
ALTER TABLE `usuario_detalle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area_laboral`
--
ALTER TABLE `area_laboral`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT de la tabla `categorias_app`
--
ALTER TABLE `categorias_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `categoria_articulo`
--
ALTER TABLE `categoria_articulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `cotizacion_detalle`
--
ALTER TABLE `cotizacion_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT de la tabla `cotizacion_maestro`
--
ALTER TABLE `cotizacion_maestro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `c_cliente`
--
ALTER TABLE `c_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `c_usuario`
--
ALTER TABLE `c_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `marca_articulo`
--
ALTER TABLE `marca_articulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `rolusuario`
--
ALTER TABLE `rolusuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `usuario_detalle`
--
ALTER TABLE `usuario_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
